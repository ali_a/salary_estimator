در بخش (کدام مورد در مورد شما صدق میکند (اطلاعات شخصی)) یک گزینه با عنوان (در رابطه نیستم ولی دوست داشتم بودم) وجود دارد. با توجه به اینکه همین سوال به صورت کامل‌تر در بخش (وضعیت رابطه) پرسیده شده، بهتر است از این سوال از بخش کدام مورد در مورد شما صدق میکند) حذف شود.

بهتر است گزینه‌ی (عینک یا لنز) به بخش (تغییرات در بدن) یا بخش (تیپ) اضافه شود.

در بخش (زبان‌های مورد استفاده) و (زبان‌های موردعلاقه جهت یادگیری)، زبان lua اضافه شود.

در بخش (زبان‌های موردعلاقه جهت یادگیری)، زبان‌های erlang و fortran اضافه شود. این دو زبان در بخش (زبان‌های مورد استفاده) ذکر شده‌اند.

از آنجایی که یک فیلد کامل به سورس کنترل ها اختصاص داده شده، وجود سورس کنترل در بخش تکنولوژی ها بیهوده است.

بخش چگونه کار میکنید به چیزی مانند (بیشترین درآمد شما به چه طریقی است) تغییر کند. زیرا یک فرد ممکن است از چند طریق درآمدزایی کند. (گزینه‌ی سایر زیاد انتخاب شده و افراد سغی کرده اند منابغ درآمدی خود را توضیح دهند.)
در بخش (چگونه کار میکنید) گزینه (فعلا پول خاصی از آی‌تی در نمیآورم) به گزینه‌ای عمومی تر مثل (فعلا برنامه نویسی/ سیستم ادمینی نمیکنم) تغییر یابد.
همینطور این گزینه از گزینه‌ی آخز، به گزینه‌ی اول تغییر یابد.

فیلدهای سن و حقوق به صورت عددی باشد