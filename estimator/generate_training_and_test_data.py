"""
a simple module to generate ml friendly input data from dataset

This module will generate a numpy array that will be used for training and
testing
"""
import pandas
import numpy
import pickle
from config.config import encoded_full_path, ml_dataset_full_path

dataset = pandas.read_csv(encoded_full_path)
print(f'dataset shape: {dataset.shape}\n')

# drop nan rows
dataset = dataset.dropna()

columns = ['age', 'work_state', 'education',
           'project_language', 'database', 'os', 'server',
           'work_field', 'workplace_type', 'co_workers']
# prepare x axis data: features
final_x = []
for row in range(len(dataset)):
    row_data = []
    for column in dataset.columns:
        if column == 'salary':
            continue
        row_data.append(dataset[column][row])
    final_x.append(row_data)

final_x = numpy.array(final_x, dtype='int')
print(f'final_x shape: {final_x.shape}')

# prepare y axis data: labels
final_y = []
for row in range(len(dataset)):
    final_y.append(dataset['salary'][row])

final_y = numpy.array(final_y, dtype='int')
print(f'final_y shape: {final_y.shape}')

# split data to training and testing parts
train_x = final_x[:4000]
train_y = final_y[:4000]
test_x = final_x[4000:]
test_y = final_y[4000:]

# show off the results
print('')
print(f'train_x shape: {train_x.shape}')
print(f'train_y shape: {train_y.shape}')
print(f'test_x shape: {test_x.shape}')
print(f'test_y shape: {test_y.shape}')

# save the result
data = (train_x, train_y), (test_x, test_y)
with open(ml_dataset_full_path, 'wb') as handle:
    pickle.dump(data, handle)
    print(f'dataset saved as: {ml_dataset_full_path}')
