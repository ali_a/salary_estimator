from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
from itertools import product
import os
import pickle
import time

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
number_of_epochs = 250
header = '-' * 120

# load training and test data
with open('./ml_dataset.pickle', 'rb') as my_file:
    (train_x, train_y), (test_x, test_y) = pickle.load(my_file)
train_y = to_categorical(train_y)
test_y = to_categorical(test_y)

# lets make sure everything as ok
print('------ ATTENTION! ------')
print(f'train x shape: {train_x.shape}')
print(f'train y shape: {train_y.shape}')
print(f'test  x shape: {test_x.shape}')
print(f'test  y shape: {test_y.shape}')
if input('\nProceed? ').lower() not in ['y', 'yes']:
    print('Abort...')
    exit()


def create_nn(neurons_architecture, activation, optimizer, loss, dropout):
    model = Sequential()
    input_shape = train_x.shape[1:]

    # add first layer. this layer is added separately because we want to define
    # input shape in it
    model.add(Dense(neurons_architecture[0],
                    activation=activation,
                    input_shape=input_shape))
    if dropout is not None:
        model.add(Dropout(dropout))

    # add the rest of layers
    for neurons in neurons_architecture[1:]:
        model.add(Dense(neurons, activation=activation))
        if dropout is not None:
            model.add(Dropout(dropout))

    # add the last layer and compile
    model.add(Dense(6, activation='softmax'))
    model.compile(optimizer=optimizer,  # adam and rmsprop
                  loss=loss,  # categorical_hinge
                  metrics=['accuracy'])

    # generate model name
    # TODO separate this job
    model_name = f'NN: {str(neuron_arc)}, '
    model_name += f'activation: {activation}, '
    model_name += f'optimizer: {optimizer}, '
    model_name += f'loss: {loss}, '
    model_name += f'dropout: {str(dropout)}'

    return model, model_name


activation_list = ['relu', 'tanh']
loss_list = ['categorical_crossentropy']
optimizer_list = ['rmsprop', 'adam']
drop_list = [None, 0.05, 0.1, 0.2, 0.4]
nn_arc_list = [
    (32, 16),
    (128, 64),
    (256, 64),
    (64, 32, 16),
    (64, 64, 32),
    (128, 32, 16),
    (1024, 256, 64),
    (1024, 256, 64, 16),
    (1024, 512, 128, 64),
    (128, 64, 32, 32, 16, 16, 8),
    (1024, 512, 256, 128, 64, 32),
    (128, 128, 64, 64, 64, 64),
    ]

all_states = product(nn_arc_list, activation_list, loss_list, optimizer_list,
                     drop_list)
with open('all_states.pickle', 'wb') as my_file:
    pickle.dump(all_states, my_file)

all_files_names = []

for counter, state in enumerate(all_states):
    neuron_arc, activation, loss = state[0], state[1], state[2]
    optimizer, dropout = state[3], state[4]
    model, model_name = create_nn(neuron_arc, activation, optimizer, loss,
                                  dropout)

    print(header)
    print(f'model: {counter}, {model_name}')

    # check existence to eliminate unnecessary training
    file_name = f'./history/{counter}-{model_name}'
    all_files_names.append(file_name)
    if os.path.exists(file_name):
        print('NN already exists. skiping.')
        continue

    before_time = time.time()
    history = model.fit(train_x, train_y, epochs=number_of_epochs,
                        batch_size=100, validation_data=(test_x, test_y),
                        shuffle=True, verbose=False)
    after_time = time.time()
    model.save(f'./history/{counter}.model')
    max_val_acc = max(history.history['val_acc'])

    # show off the result
    print(f'max acc val: {max_val_acc}')
    print(f'time: {after_time - before_time} seconds')

    # save the history

    with open(file_name, 'wb') as my_file:
        pickle.dump(history, my_file)
    # create_plot(history, file_name)

with open('all_files_names.pickle', 'wb') as my_file:
    pickle.dump(all_files_names, my_file)
