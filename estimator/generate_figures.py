import matplotlib.pyplot as plt
import pickle
from os.path import exists


def create_plot(history, name):
    epochs = range(250)
    plt.figure(figsize=(10, 4))

    # losses
    plt.subplot(1, 2, 1)
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    plt.plot(epochs, loss, 'b', label='Training loss')
    plt.plot(epochs, val_loss, 'r', label='Validation loss')
    plt.xlabel('Epochs')
    plt.title('Loss')
    plt.ylabel('Loss')
    plt.legend()

    # accuracy
    plt.subplot(1, 2, 2)
    acc = history.history['acc']
    val_acc = history.history['val_acc']

    plt.plot(epochs, acc, 'b', label='Training acc')
    plt.plot(epochs, val_acc, 'r', label='Validation acc')
    plt.xlabel('Epochs')
    plt.title('Accuracy')
    plt.ylabel('Accuracy')
    plt.legend()

    # save the figure and clear plotter
    plt.suptitle(f'best val acc: {max(val_acc) * 100}%')
    plt.savefig(f'{name}.png', dpi=500)
    plt.clf()
    plt.close()


def job(file_name):
    if exists(f'{file_name}.png'):
        print(f'{file_name} already exists. skiping.')
        return
    print(f'loading history: {file_name}')
    with open(file_name, 'rb') as my_file:
        history = pickle.load(my_file)
    create_plot(history, file_name)


with open('all_files_names.pickle', 'rb') as my_file:
    all_files_names = pickle.load(my_file)

for file_name in all_files_names:
    job(file_name)
