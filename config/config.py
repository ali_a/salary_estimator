from os.path import join
from tools.utils import complex_namespace

# base paths
_project_dir = '/home/ali/Projects/salary_estimator'

# estimator paths
_estimator = join(_project_dir, 'estimator/')
_estimator_dataset = join(_estimator, 'ml_dataset.pickle')
_label_encoders = join(_estimator, 'label_encoders.pickle')
_standard_scaler = join(_estimator, 'standard_scaler.pickle')

# dataset paths
_dataset = join(_project_dir, 'dataset/')
_dataset_renamed = join(_dataset, 'dataset_renamed_97.csv')
_dataset_encoded = join(_dataset, 'dataset_encoded.csv')
_dataset_reduced = join(_dataset, 'dataset_reduced.csv')

# alalysis paths
_analysis = join(_project_dir, 'analysis/')
_pca = join(_analysis, 'pca/')
_pca_result = join(_pca, 'results/')
_corr = join(_analysis, 'correlation/')
_corr_result = join(_corr, 'results/')

# values
max_digital_devices = 20
max_co_workers = 10_000
no_answer = 'zzz_no_answer'

# all the paths
_paths_dict = {
    'base': _project_dir,
    'estimator': {
        'base': _estimator,
        'ml_dataset': _estimator_dataset,
        'standard_scalers': _standard_scaler,
        'label_encoders': _label_encoders,
    },
    'dataset': {
        'base': _dataset,
        'renamed': _dataset_renamed,
        'reduced': _dataset_reduced,
        'encoded': _dataset_encoded,
    },
    'analysis': {
        'base': _analysis,
        'pca': {
            'base': _pca,
            'results': _pca_result,
        },
        'correlation': {
            'base': _corr,
            'results': _corr_result,
        }
    },
}

paths = complex_namespace(_paths_dict)
