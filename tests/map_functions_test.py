from pytest import raises
from numpy import nan
from data_structures import data_structures
from tools import map_functions
from config.config import max_digital_devices, max_co_workers


big_number = 999999999999999999999999999999999999999999999999999999999999999999


def test_map_digital_devices_for_max_value():
    """
    there are 5 values for digital_devices:
    ["one", "two", "three", "four", "more"]
    so we need max_digital_devices to be greater that 4
    # """
    assert max_digital_devices > 4


def test_map_digital_devices_with_negative_values():
    func = map_functions.map_digital_devices
    with raises(AssertionError):
        func(-1)
        func(-2)
        func(-1000)
        func(-1 * big_number)


def test_map_digital_devices_with_large_values():
    """
    check numbers that excede maximum value (in config: max_digital_devices)
    """
    func = map_functions.map_digital_devices
    with raises(AssertionError):
        func(max_digital_devices + 1)
        func(max_digital_devices + 2)
        func(max_digital_devices + 1000)
        func(max_digital_devices + big_number)


def test_map_digital_devices_with_main_values():
    func = map_functions.map_digital_devices
    assert func(0) == "0"
    assert func(1) == "1"
    assert func(2) == "2"
    assert func(3) == "3"
    assert func(4) == "4"


def test_map_digital_devices_with_other_values():
    func = map_functions.map_digital_devices
    for i in range(5, max_digital_devices+1):
        assert func(i) == "5"


def test_map_digital_devices_with_nan_value():
    func = map_functions.map_digital_devices
    assert func(nan) == "0"


def test_map_digital_devices_with_fractional_values():
    func = map_functions.map_digital_devices
    with raises(AssertionError):
        func(0.00000000000000001)
        func(0.9999999999999999)
        func(0.5)
        func(1.3)
        func(3.008)
        func(-0.00000000000000001)
        func(-0.9999999999999999)
        func(-0.5)


def est_map_digital_devices_with_strings():
    func = map_functions.map_digital_devices
    with raises(ValueError):
        func("")
        func("string")


def test_map_co_workers_with_negative_number():
    func = map_functions.map_co_workers
    with raises(AssertionError):
        func(-1)
        func(-2)
        func(-1000)
        func(-1 * big_number)


def test_map_co_workers_with_basic_values():
    func = map_functions.map_co_workers
    assert func(0) == "0"
    assert func(1) == "0"
    assert func(2) == "0"


def test_map_co_workers_with_3_to_10_coworkers():
    """check values for very small companies: 3 to 10 co_workers"""
    func = map_functions.map_co_workers
    for number_of_co_workers in range(3, 10+1):
        assert func(number_of_co_workers) == "1"


def test_map_co_workers_with_11_to_50_coworkers():
    """check values for small companies: 11 to 50 co_workers"""
    func = map_functions.map_co_workers
    for number_of_co_workers in range(11, 50+1):
        assert func(number_of_co_workers) == "2"


def test_map_co_workers_with_51_to_200_coworkers():
    """check values for medium companies: 51 to 200 co_workers"""
    func = map_functions.map_co_workers
    for number_of_co_workers in range(51, 200+1):
        assert func(number_of_co_workers) == "3"


def test_map_co_workers_with_201_to_1000_coworkers():
    """check values for large companies: 201 to 1000 co_workers"""
    func = map_functions.map_co_workers
    for number_of_co_workers in range(201, 1000+1):
        assert func(number_of_co_workers) == "4"


def test_map_co_workers_with_1001_to_10000_coworkers():
    """check values for huge companies: 1001 to 10000 co_workers"""
    func = map_functions.map_co_workers
    for number_of_co_workers in range(1001, max_co_workers + 1):
        assert func(number_of_co_workers) == "4"


def test_map_co_workers_with_values_larger_that_maximum_allowed():
    """check numbers that excede maximum value (in config: max_co_workers)"""
    func = map_functions.map_co_workers
    with raises(AssertionError):
        func(max_co_workers + 1)
        func(max_co_workers + 2)
        func(max_co_workers + 1000)
        func(max_co_workers + big_number)


def test_map_co_workers_with_nan_values():
    func = map_functions.map_co_workers
    result = func(nan)
    assert result == "0"


def test_map_co_workers_with_fractional_values():
    func = map_functions.map_co_workers
    with raises(AssertionError):
        func(0.00000000000000001)
        func(0.9999999999999999)
        func(0.5)
        func(1.3)
        func(3.008)
        func(-0.00000000000000001)
        func(-0.9999999999999999)
        func(-0.5)


def test_map_co_workers_with_strings():
    func = map_functions.map_co_workers
    with raises(ValueError):
        func("")
        func("string")


def test__check_if_any_with_some_values_to_return_false():
    func = map_functions._check_if_any
    small_list = ['java', 'c', 'c++']
    big_list = ['bird', 'monkey', 'racon', 'dog', 'cat']
    assert not func(small_list, big_list)


def test__check_if_any_with_some_values_to_return_true():
    func = map_functions._check_if_any
    small_list = ['java', 'c', 'c++']
    big_list = ['bird', 'monkey', 'racon', 'dog', 'cat', 'c++']
    assert func(small_list, big_list)
    assert func(small_list, small_list)
    assert func(small_list, ['c'])


def test__check_if_any_with_empty_small_list():
    func = map_functions._check_if_any
    big_list = ['bird', 'monkey', 'racon', 'dog', 'cat']
    with raises(ValueError):
        func([], big_list)


def test__check_if_any_with_empty_big_list():
    func = map_functions._check_if_any
    small_list = ['java', 'c', 'c++']
    with raises(ValueError):
        func(small_list, [])


def test_map_project_language_with_some_values():
    structures = data_structures.language
    pl_func = map_functions.create_binary_map_function(**structures)
    pl_tuple = structures['structure_tuple']

    languages = 'c, c++, c#, d, python, r'
    my_tuple = pl_tuple(1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0)
    result = pl_func(languages)
    assert my_tuple == result


def test_map_project_language_with_one_multi_choice_values():
    structures = data_structures.language
    pl_func = map_functions.create_binary_map_function(**structures)
    pl_tuple = structures['structure_tuple']
    my_tuple = pl_tuple(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

    languages = 'c'
    result = pl_func(languages)
    assert my_tuple == result

    languages = 'c++'
    result = pl_func(languages)
    assert my_tuple == result

    languages = 'asmb'
    result = pl_func(languages)
    assert my_tuple == result

    languages = 'asmb, c++'
    result = pl_func(languages)
    assert my_tuple == result

    languages = 'asmb, c, c++'
    result = pl_func(languages)
    assert my_tuple == result


def test_map_project_language_with_empty_string_as_input():
    structures = data_structures.language
    pl_func = map_functions.create_binary_map_function(**structures)
    pl_tuple = structures['structure_tuple']
    my_tuple = pl_tuple(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    languages = ''
    result = pl_func(languages)
    assert my_tuple == result


def test_map_project_language_with_nan_as_input():
    structures = data_structures.language
    pl_func = map_functions.create_binary_map_function(**structures)
    pl_tuple = structures['structure_tuple']
    my_tuple = pl_tuple(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    languages = nan
    result = pl_func(languages)
    assert my_tuple == result
