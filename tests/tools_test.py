"""A simple module to test encode_dataset."""

import pytest
import pandas
from numpy import nan
from tools import tools


def test_encode_data_with_one_valid_data():
    """Test encode_data with valid data."""
    data = 'beer'
    encode_list = ['tea', 'coffe', 'energ', 'beer', 'alcoh', 'water']
    mask = '_' * len(encode_list)
    result = tools.encode_data(data, encode_list, mask)
    assert result == '"000100"'


def test_encode_data_with_two_valid_data():
    """Test encode_data with valid data."""
    data = 'beer, water'
    encode_list = ['tea', 'coffe', 'energ', 'beer', 'alcoh', 'water']
    mask = '_' * len(encode_list)
    result = tools.encode_data(data, encode_list, mask)
    assert result == '"000101"'


def test_encode_data_with_two_valid_data_and_junks():
    """Test encode_data with valid data."""
    data = 'tea, alcoh, apple juice, tomato'
    encode_list = ['tea', 'coffe', 'energ', 'beer', 'alcoh', 'water']
    mask = '_' * len(encode_list)
    result = tools.encode_data(data, encode_list, mask)
    assert result == '"100010"'


def test_encode_data_with_junk_data():
    """Test encode_data with invalid data."""
    data = 'some invalid data, hello there, third data piece'
    encode_list = ['coffee', 'cow', 'resistors', 'golf', 'satan', 'blood']
    mask = '_' * len(encode_list)
    result = tools.encode_data(data, encode_list, mask)
    assert result == '"000000"'


def test_encode_data_with_empty_data_and_nan_to_zero_set_to_true():
    """Test encode_data with empty data and nan_to_zero set to True."""
    data = ''
    encode_list = ['coffee', 'cow', 'resistors', 'golf', 'satan', 'blood']
    mask = '_' * len(encode_list)
    result = tools.encode_data(data, encode_list, mask, True)
    assert result == '"000000"'


def test_encode_data_with_empty_data_and_nan_to_zero_set_to_false():
    """Test encode_data with empty data and nan_to_zero set to False."""
    data = ''
    encode_list = ['coffee', 'cow', 'resistors', 'golf', 'satan', 'blood']
    mask = '_' * len(encode_list)
    result = tools.encode_data(data, encode_list, mask, False)
    assert result == '"000000"'


def test_encode_data_with_nan_data_and_nan_to_zero_set_to_true():
    """Test encode_data with nan data and nan_to_zero set to True."""
    data = nan
    encode_list = ['coffee', 'cow', 'resistors', 'golf', 'satan', 'blood']
    mask = '_' * len(encode_list)
    result = tools.encode_data(data, encode_list, mask, True)
    assert result == '"000000"'


def test_encode_data_with_nan_data_and_nan_to_zero_set_to_false():
    """Test encode_data with nan data and nan_to_zero set to False."""
    data = nan
    encode_list = ['coffee', 'cow', 'resistors', 'golf', 'satan', 'blood']
    mask = '_' * len(encode_list)
    result = tools.encode_data(data, encode_list, mask, False)
    assert result is nan


def test_encode_data_with_empty_list():
    """Test encode_data with empty list."""
    data = 'some, data, here'
    encode_list = []
    mask = '_' * len(encode_list)
    with pytest.raises(IndexError):
        _ = tools.encode_data(data, encode_list, mask)


def test_encode_data_with_double_quotes_set_to_false():
    """Test encode_data with double_quotes set to False."""
    data = 'a, b, c, d'
    encode_list = ['a', 'b', 'c']
    mask = '_' * len(encode_list)
    result = tools.encode_data(data, encode_list, mask,
                               add_double_quotes=False)
    assert result == '111'

    data2 = 'a, b, g , f, r'
    result2 = tools.encode_data(data2, encode_list, mask,
                                add_double_quotes=False)
    assert result2 == '110'


def test_encode_pandas_series_with_single_values():
    """Test encode_pandas_series with rows of single value."""
    data = ['rabbit', 'owl', 'lizard', 'spyder', 'PYTHON']
    my_series = pandas.Series(data)
    result = tools.encode_pandas_series(my_series, data)
    assert result[0] == '"10000"'
    assert result[1] == '"01000"'
    assert result[2] == '"00100"'
    assert result[3] == '"00010"'
    assert result[4] == '"00001"'


def test_encode_pandas_series_with_multi_values():
    """Test encode_pandas_series with rows of multiple values."""
    data = ['rabbit', 'rabbit, owl', 'lizard', '', 'PYTHON, tomato, spyder',
            'some, other, animals', ',']
    encode_list = ['rabbit', 'owl', 'lizard', 'spyder', 'PYTHON']
    my_series = pandas.Series(data)
    result = tools.encode_pandas_series(my_series, encode_list)
    assert result[0] == '"10000"'
    assert result[1] == '"11000"'
    assert result[2] == '"00100"'
    assert result[3] == '"00000"'
    assert result[4] == '"00011"'
    assert result[5] == '"00000"'
    assert result[6] == '"00000"'


def test_encode_pandas_series_with_empty_encode_list():
    """Test encode_pandas_series with empty encode_list."""
    data = ['rabbit', 'rabbit, owl', 'lizard', '', 'PYTHON, tomato, spyder']
    encode_list = []
    my_series = pandas.Series(data)
    with pytest.raises(IndexError):
        _ = tools.encode_pandas_series(my_series, encode_list)


def test_encode_pandas_series_with_nans():
    """Test encode_pandas_series with a series that have NaNs in it."""
    encode_list = ['zip', 'rar', 'gz']
    data_series = pandas.Series(['zip, rar', '', nan, 'gz, lib, dll'])
    r1 = tools.encode_pandas_series(data_series, encode_list, False)
    r2 = tools.encode_pandas_series(data_series, encode_list, True)

    assert r1[0] == '"110"'
    assert r2[0] == '"110"'
    assert r1[1] == '"000"'
    assert r2[1] == '"000"'
    assert r1[2] is nan
    assert r2[2] == '"000"'
    assert r1[3] == '"001"'
    assert r2[3] == '"001"'
