from config import config as cfg
from collections import namedtuple
from pandas import isnull, Series
from numpy import nan


# TODO remove this function
def _check_range(number, minimum, maximum):
    """
    Check if the given number is between max and min.
    """
    if minimum-1 < number < maximum+1:
        return True
    return False


def encode_co_workers(number_of_co_workers):
    """
    Map integer values to categorical string values.

    nan is mapped "0"
    0 is mapped to "0"
    1 is mapped to "1"
    2 is mapped to "2"
    3 to 10 is mapped to "3"
    11 to 50 is mapped to "4"
    51 to 200 is mapped to "5"
    201 to 1000 is mapped to "6"
    1001 to 10000 (max value) is mapped to "7"

    Parameters
    ----------
    number_of_co_workers : float or int
        indicated number of co_workers

    Returns
    -------
    string

    Raises
    ------
    AssertionError
        when number is negative or exceeds max value (in config file)
        when number has a fraction part
    ValueError
        when the given input is not a int or float
    """
    if number_of_co_workers == "":
        raise ValueError

    # handle empty/nan values
    if isnull(number_of_co_workers):
        return "0"

    # raise ValueError if number_of_co_workers in not a float or int
    floated_number = float(number_of_co_workers)

    # raise AssertionError if tne number have fraction part
    msg = f"The number cannot have a fraction. got: {number_of_co_workers}"
    assert floated_number.is_integer(), msg

    msg = "negative number received. expected a positive number"
    msg += f" got: {number_of_co_workers}"
    assert number_of_co_workers > -1, msg

    msg = f"too many co_workers. cannot excede {cfg.max_co_workers}."
    msg += f" got: {number_of_co_workers}"
    assert number_of_co_workers <= cfg.max_co_workers, msg

    if number_of_co_workers in [0, 1, 2]:
        # 0 -> 1 row
        # 1 -> 1085 rows
        # 2 -> 149 rows
        return "0"

    company_detail = namedtuple('company_detail', ['size', 'min', 'max'])
    company_details = [
        company_detail('1', 3, 10),
        company_detail('2', 11, 50),
        company_detail('3', 51, 200),
        company_detail('4', 201, 1000),
        company_detail('4', 1001, cfg.max_co_workers),  # 104 rows
    ]

    for detail in company_details:
        if _check_range(number_of_co_workers, detail.min, detail.max) is True:
            return detail.size


def encode_digital_devices(number_of_devices):
    """
    Map integer values to categorical string values.

    nan is mapped to "0"
    0 is mapped to "0".
    1 is mapped to "1".
    2 is mapped to "2".
    3 is mapped to "3".
    4 is mapped to "4".
    other numbers are mapped to "5"

    Parameters
    ----------
    number_of_devices : float or int
        indicated number of figital devices

    Returns
    -------
    string

    Raises
    ------
    AssertionError
        when number is negative or exceeds max value (in config file)
        when number has a fraction part
    ValueError
        when the given input is not a int or float
    """
    # handle empty/nan values
    if isnull(number_of_devices):
        return '0'

    # raise ValueError if number_of_devices in not a float or int
    floated_number = float(number_of_devices)

    # raise AssertionError if tne number have fraction part
    msg = f"The number has a fraction. got: {number_of_devices}"
    assert floated_number.is_integer(), msg

    msg = "negative number received. expected a positive number"
    msg += f" got: {number_of_devices}"
    assert number_of_devices > -1, msg

    msg = f"too many devices. cannot excede {cfg.max_digital_devices}."
    msg += f" got: {number_of_devices}"
    assert number_of_devices <= cfg.max_digital_devices, msg

    devices = {
        0: '0',
        1: '1',
        2: '2',
        3: '3',
        4: '4'
    }
    return devices.get(number_of_devices, '5')


def encode_data(data: str, encode_list: list, mask: str,
                nan_to_zero: bool = True, add_double_quotes: bool = True):
    """
    Encode one piece of data.

    Raise IndexError if encode_list is empty.

    Args:
    -----
    data : the data we are trying to endode.

    encode_list : the list we encode our data with respect to it

    mask : a string with length of final encoded data. we change the mask
    while prossecing our data so it will represent our encoded data.

    nan_to_zero : if True then all nan values are encoded to a bunch of zeros.
    if False, nan values will remain nan.

    add_double_quotes : add double quotes at beggining and end of final string.
    writing a bunch of zeros onto a csv file will cause them to appear as one
    single zero. with add_double_quotes set to True (default), the mentioned
    problem will not happen.

    Examples:
    --------
    >>> data = 'beer, water, Do NOT include this example in your data.'

    >>> encode_list = ['tea', 'coffe', 'energ', 'beer', 'alcoh', 'water']

    >>> mask = '_' * len(encode_list)  # mask = '______'

    >>> r = encode_data(data, encode_list, mask)

    >>> print(r)

    '"000101"'

    >>> r = encode_data(data, encode_list, mask, add_double_quotes=False)

    >>> print(r)

    '000101'

    >>> data = numpy.nan

    >>> encode_list = ['some', 'options']

    >>> mask = '_' * len(encode_list)

    >>> r1 = encode_data(data, encode_list, mask, True)

    >>> print(r1)

    '"00"'

    >>> r2 = encode_data(data, encode_list, mask, True, False)

    >>> print(r2)

    '00'

    >>> r3 = encode_data(data, encode_list, mask, False)

    >>> print(r3)

    nan

    >>> r4 = encode_data(data, encode_list, mask, False, True)

    >>> print(r4)

    nan

    """
    # raise IndexError when the encode_list is empty
    if encode_list == []:
        raise IndexError
    # if data is nan, decide what to do whit it:
    if data is nan and nan_to_zero is True:
        mask = '0' * len(encode_list)
        # add double quotes if needed
        mask = '"' + mask + '"' if add_double_quotes else mask
        return mask
    if data is nan and nan_to_zero is False:
        return nan
    # data is a string and we need a list to iterate through it
    list_of_data = data.split(', ')
    for num, item in enumerate(encode_list):
        flag = '1' if item in list_of_data else '0'
        mask = mask[:num] + flag + mask[num+1:]

    # add double quotes if needed
    mask = '"' + mask + '"' if add_double_quotes else mask
    return mask


def encode_pandas_series(pandas_series: Series,
                         encode_list: list, nan_to_zero: bool = True):
    """
    Encode the given pandas_series to a binary-like string.

    Return a pandas series with encoded data

    Args:
    -----
    pandas_series : the series we are trying to encode.

    encode list: a list of elements we desire to encode our data with respect
    to it.

    nan_to_zero: if True then all nan values are encoded to a bunch of zeros.
    if False, nan values will remain nan.

    Examples:
    --------
    >>> data = ['rabbit', 'rabbit, owl', 'lizard', '',
        'PYTHON, tomato, spyder', 'some, other, animals', ',']

    >>> encode_list = ['rabbit', 'owl', 'lizard', 'spyder', 'PYTHON']

    >>> my_series = pandas.Series(data)

    >>> result = encode_dataset.encode_pandas_series(my_series, encode_list)

    >>> print(result)

    0    10000
    1    11000
    2    00100
    3    00000
    4    00011
    5    00000
    6    00000
    dtype: object

    >>> my_series = pandas.Series(['owl, lizard', '', nan, 'dummy values, d'])

    >>> encode_list = ['rabbit', 'owl', 'lizard', 'spyder', 'PYTHON']

    >>> print(my_series)

    0        owl, lizard
    1
    2                NaN
    3    dummy values, d
    dtype: object

    >>> result = encode_pandas_series(my_series, encode_list, False)

    >>> print(result)

    0    01100
    1    00000
    2      NaN
    3    00000
    dtype: object

    >>> result = encode_pandas_series(my_series, encode_list, True)

    >>> # or: result = encode_pandas_series(my_series, encode_list)

    >>> print(result)

    0    01100
    1    00000
    2    00000
    3    00000
    dtype: object

    """
    final_list = []
    # mask is a string of ones and zeroes but for now, as a temporary value, we
    # fill it with underscores.
    # mask is a template for our encoded result. in each iteration, we will
    # copy it into encoding_result.
    mask = '_' * len(encode_list)
    for data in pandas_series:
        encoding_result = encode_data(data, encode_list, mask, nan_to_zero)
        final_list.append(encoding_result)
    return Series(final_list)
