"""Diffrent encoders for diffrent data columns"""
from collections import namedtuple
from pandas import isnull, Series, DataFrame
from sklearn.preprocessing import LabelEncoder, StandardScaler
import pickle


# TODO remove this function
def _check_if_any(small_list, big_list):
    """Check if any element of small_list exists in the big_list

    Parameters
    ----------
    small_list : list
    big_list : list

    Returns
    -------
    boolean

    Raises:
    -------
    ValueError if any of inputs are empty lists

    Examples:
    ---------
    >>> small_list = ['java', 'c', 'c++']
    >>> big_list = ['bird', 'monkey', 'racon', 'dog', 'cat', 'c++']
    >>> _check_if_any(small_list, big_list)
    True
    """
    if small_list == [] or big_list == []:
        raise ValueError
    return any(i in big_list for i in small_list)


def create_binary_map_function(columns_dictionary: dict, master_name: str):
    """Create a function that will accept a string and map it to some value

    The internal function will take a string and map the string to a
    namedtuple. This is done with the help of a list: columns_list and a
    dictionary: columns_special_cases.

    Parameters
    ----------
    columns_dictionary : dict
        the keys are some values in columns_list. the values indicates what
        should be mapped to its coresponding key.
    master_name : str
        a string used as our namedtuple's name and a prefix to all the columns
        it will produce.

    Returns
    -------
    function
        this function will take a string and map it to some namedtuple

    Examples
    --------
    >>> languages = {
    ...     'c_cpp_asmb': ['c', 'c++', 'asmb'],
    ...     'bash': ['bash'],
    ...     'c_sharp': ['c#'],
    ...     'java': ['java'],
    ...     'swift_obj_c': ['swift', 'obj_c'],
    ... }

    >>> map_f = create_binary_map_function(languages, 'languages')

    >>> map_f('')
    languages(languages_c_cpp_asmb=0, languages_bash=0, languages_c_sharp=0,
              languages_java=0, languages_swift_obj_c=0)

    >>> map_f('c, c++')
    languages(languages_c_cpp_asmb=1, languages_bash=0, languages_c_sharp=0,
              languages_java=0, languages_swift_obj_c=0)

    >>> map_f('c')
    languages(languages_c_cpp_asmb=1, languages_bash=0, languages_c_sharp=0,
              languages_java=0, languages_swift_obj_c=0)

    >>> map_f('matlab, c++, gen, python, js, c#, cobol, vb')
    languages(languages_c_cpp_asmb=1, languages_bash=0, languages_c_sharp=1,
              languages_java=0, languages_swift_obj_c=0)
    """
    def map_function(input_string: str):
        # column_names is columns_dictionary's keys prefixed with master_name
        prefix = master_name + '_'
        column_names = [prefix + name for name in columns_dictionary.keys()]
        costume_namedtuple = namedtuple(master_name, column_names)

        if isnull(input_string):
            empty = [0] * (len(columns_dictionary))
            return costume_namedtuple(*empty)

        result = {}
        input_string_list = input_string.split(', ')

        for key, values in columns_dictionary.items():
            name = prefix + key
            result[name] = 1 if _check_if_any(values, input_string_list) else 0

        return costume_namedtuple(**result)

    return map_function


# TODO: docstring split_and_encode_column
def split_and_encode_column(raw_dataset: DataFrame, column_name: str,
                            function):
    column = raw_dataset[column_name]
    languages_tuple = list(map(function, column))
    columns_to_create = languages_tuple[0]._fields

    for index, new_column_name in enumerate(columns_to_create):
        new_column = [languages[index] for languages in languages_tuple]
        raw_dataset[new_column_name] = Series(new_column)

    raw_dataset.drop(columns=column_name, inplace=True)


class LabelEncoders:
    def __init__(self):
        self.encoders = {}

    def new_encoder(self, name: str):
        encoder = LabelEncoder()
        self.encoders[name] = encoder
        return encoder

    def get_encoder(self, name: str):
        return self.encoders.get(name, None)

    def save_encoders(self, path: str):
        with open(path, 'wb') as my_file:
            pickle.dump(self.encoders, my_file)

    def load_encoders(self, path: str):
        with open(path, 'rb') as my_file:
            self.encoders = pickle.load(my_file)


class StandardScalers:
    def __init__(self):
        self.scalers = {}

    def new_scaler(self, name: str):
        scaler = StandardScaler()
        self.scalers[name] = scaler
        return scaler

    def get_scaler(self, name: str):
        return self.scalers.get(name, None)

    def save_scalers(self, path: str):
        with open(path, 'wb') as my_file:
            pickle.dump(self.scalers, my_file)

    def load_scalers(self, path: str):
        with open(path, 'rb') as my_file:
            self.scalers = pickle.load(my_file)
