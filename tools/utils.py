"""useful tools"""
from types import SimpleNamespace


def complex_namespace(dictionary: dict):
    """Create a namespace of given dictionary

    the difference between complex_namespace and python's types.SimpleNamespace
    is that the former will create name space recursively, but the later will
    create the namespace in one layer indentation. See the examples to see the
    difference.

    Parameters
    ----------
    dictionary : dict
        A dict to be converted to a namespace object

    Returns
    -------
    types.SimpleNamespace
        A combination of SimpleNamespaces that will have an multilayer
        namespace

    Examples
    --------
    >>> dictionary = {
    ...     'layer1_a': '1a',
    ...     'layer1_b': {
    ...         'layer2_a': '2a',
    ...     },
    ... }

    >>> # types.SimpleNamespace
    >>> simple = SimpleNamespace(**dictionary)
    >>> simple.layer1_a
    '1a'
    >>> simple.layer1_b.layer2_a
    AttributeError: 'dict' object has no attribute 'layer2_a'
    # because layer1_b is still a dictionary

    >>> # complex_namespace
    >>> complex = complex_namespace(dictionary)
    >>> complex.layer1_a
    '1a'
    >>> complex.layer1_a.layer2_a
    '2a'
    """
    space = {}
    for key, value in dictionary.items():
        if isinstance(value, dict):
            value = complex_namespace(value)
        space[key] = value
    return SimpleNamespace(**space)


def show_details(pandas_dataframe, index: int, metrics=None):
    """A simple function to show information about a user in dataset"""
    if metrics is None:
        metrics = ['age', 'salary', 'project_language', 'primary_job',
                   'co_workers', 'digital_devices']
    row = pandas_dataframe.loc[index]
    for i in metrics:
        print(f'{i}: {row[i]}', end=', ')
    print('')
