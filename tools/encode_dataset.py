"""A module to encode our dataset."""
import pickle
from numpy import where
from pandas import read_csv, isnull, DataFrame
from config.config import paths
from config.config import max_co_workers, no_answer
from encoders import LabelEncoders, create_binary_map_function, StandardScalers
from encoders import split_and_encode_column
from data_structures.data_structures import splitable_columns
from data_structures.data_structures import splitable_columns_reduced
from data_structures.data_structures import labels_and_default_values


def show_special_cases(raw_dataset: DataFrame):
    from special_encoders import encode_co_workers, encode_digital_devices
    from utils import show_details
    header = '-' * 80

    # special cases for co_workers
    print('co_workers:')
    print(header)
    co_workers = raw_dataset['co_workers']
    for index, co in enumerate(co_workers):
        try:
            _ = encode_co_workers(co)
        except AssertionError as ae:
            print(f'index: {index}. msg: {ae}')
            show_details(raw_dataset, index)
            print(header)

    # special cases for digital_devices
    print(header)
    print('digital_devices:')
    print(header)
    digital_devices = raw_dataset['digital_devices']
    for index, dd in enumerate(digital_devices):
        try:
            _ = encode_digital_devices(dd)
        except AssertionError as ae:
            print(f'index: {index}. msg: {ae}')
            show_details(raw_dataset, index)
            print(header)


def handle_special_cases_for_1397(raw_dataset: DataFrame):

    # ****** handle co_workers ******

    # index: 1144. msg: too many co_workers. cannot excede 10000. got: 15000.0
    # age: 30-39, salary: 10-13, project_language: "00001000001000000000110000"
    # primary_job: yes, co_workers: 15000.0, digital_devices: 5.0
    # decision: set co_workers to max
    raw_dataset.loc[1144, 'co_workers'] = max_co_workers

    # index: 1148. msg: The number cannot have a fraction. got: 0.99
    # age: 23-29, salary: 00-01, project_language: "00110000000100000100000000"
    # primary_job: yes, co_workers: 0.99, digital_devices: 2.0
    # decision: set co_workers to 0
    raw_dataset.loc[1148, 'co_workers'] = 0

    """deleted by hand"""
    # index: 1470. msg: The number cannot have a fraction. got: 0.34
    # age: 50-99, salary: 13-99, project_language: "00000000000000000000000001"
    # primary_job: no, co_workers: 0.34, digital_devices: 99.0
    # decision: delete row
    # raw_dataset.drop(1470, inplace=True)

    # index: 1606. msg: too many co_workers. cannot excede 10000. got: 50000.0
    # age: 23-29, salary: 13-99, project_language: "10001000100000000100010000"
    # primary_job: yes, co_workers: 50000.0, digital_devices: 3.0
    # decision: set co_workers to max
    raw_dataset.loc[1606, 'co_workers'] = max_co_workers

    # index: 1837. msg: too many co_workers. cannot excede 10000.
    # got: 1000000000000000.0
    # age: 23-29, salary: 05-07, project_language: "10110000100000000000000000"
    # primary_job: yes, co_workers: 1000000000000000.0, digital_devices: 2.0
    # decision: set co_workers to max
    raw_dataset.loc[1837, 'co_workers'] = max_co_workers

    # index: 3364. msg: too many co_workers. cannot excede 10000. got: 19000.0
    # age: 40-49, salary: 05-07, project_language: "10010000100000100100000000"
    # primary_job: yes, co_workers: 19000.0, digital_devices: 1.0
    # decision: set co_workers to max
    raw_dataset.loc[3364, 'co_workers'] = max_co_workers

    # index: 3836. msg: too many co_workers. cannot excede 10000. got: 15000.0
    # age: 40-49, salary: 2-3.5, project_language: "01101000000000000000000000"
    # primary_job: no, co_workers: 15000.0, digital_devices: 2.0
    # decision: set co_workers to max
    raw_dataset.loc[3836, 'co_workers'] = max_co_workers

    """deleted by hand"""
    # index: 3941. msg: too many co_workers. cannot excede 10000.
    # got: 2000000000.0
    # age: 00-18, salary: 13-99, project_language: "11111111111110101111111110"
    # primary_job: yes, co_workers: 2000000000.0, digital_devices: 2.0
    # decision: delete row
    # raw_dataset.drop(3941, inplace=True)

    # index: 4121. msg: too many co_workers. cannot excede 10000. got: 20000.0
    # age: 30-39, salary: 2-3.5, project_language: "00010000100000000100000000"
    # primary_job: yes, co_workers: 20000.0, digital_devices: 2.0
    # decision: set co_workers to max
    raw_dataset.loc[4121, 'co_workers'] = max_co_workers

    # ****** handle digital_devices ******

    # index 1470 was handled before

    # index: 3199. msg: The number has a fraction. got: 1.5
    # age: 30-39, salary: 3.5-5, project_language: "00010000100000000000000000"
    # primary_job: no, co_workers: 50.0, digital_devices: 1.5
    # decision: set digital_devices to 1
    raw_dataset.loc[3199, 'digital_devices'] = 1


def handle_special_cases(raw_dataset):
    # shows the row that need manual editing due to nature of its content
    # show_special_cases(raw_dataset)  # this is a commented code

    # handle special cases by changig values or dropping rows
    handle_special_cases_for_1397(raw_dataset)
    # show_special_cases(raw_dataset)  # this is a commented code


def encode_dataset_97():
    print(f'dataset located at: {paths.dataset.renamed}')
    raw_dataset = read_csv(paths.dataset.renamed)

    handle_special_cases(raw_dataset)

    # map labeled columns
    # columns with None as default value have no null value in dataset so we
    # don't need to worry about their emptiness. other columns are whether
    # filled with some kind of 0 value or a no_answer label that indicated this
    # value was null before.
    labels = LabelEncoders()
    for column_name, default_value in labels_and_default_values.items():
        print(f'labeling: {column_name}')
        encoder = labels.new_encoder(column_name)
        column = raw_dataset[column_name]
        if default_value is not None:
            column = where(isnull(column), default_value, column)
        encoder.fit(column)
        raw_dataset[column_name] = encoder.transform(column)

    # save label encoders
    with open(paths.estimator.label_encoders, 'wb') as my_file:
        pickle.dump(labels, my_file)
    print(f'label encoders saved as {paths.estimator.label_encoders}')

    # map multi_choice column and split them
    for column_name, data_structure in splitable_columns.items():
        print(f'spliting: {column_name}')
        func = create_binary_map_function(data_structure, column_name)
        split_and_encode_column(raw_dataset, column_name, func)

    # digital_devices have null values. we will fill them with 0
    dd_column = raw_dataset['digital_devices']
    new_dd = where(dd_column == no_answer, 0.0, dd_column)
    raw_dataset['digital_devices'] = new_dd

    # scale numerical values
    scalers = StandardScalers()
    scalable_columns = {
        'digital_devices': 0,
        'satisfaction': None,
        'co_workers': None,
    }
    for column_name, default_value in scalable_columns.items():
        print(f'scaling: {column_name}')
        scaler = scalers.new_scaler(column_name)
        column = raw_dataset[column_name]
        if default_value is not None:
            column = where(isnull(column), default_value, column)
            raw_dataset[column_name] = column
        new_name = column_name + '_scaled'
        raw_dataset[new_name] = scaler.fit_transform(DataFrame(column))

    # save the scalers
    with open(paths.estimator.standard_scalers, 'wb') as my_file:
        pickle.dump(scalers, my_file)
    print(f'standard scalers saved as: {paths.estimator.standard_scalers}')

    # remove redundant columns
    raw_dataset.drop(columns=['Timestamp'], inplace=True)

    # save the result
    raw_dataset.to_csv(paths.dataset.encoded, index=False)
    print(f'encoded dataset saved in: {paths.dataset.encoded}')


def encode_and_reduce_dataset_97():
    encoded_dataset = read_csv(paths.dataset.encoded)
    raw_dataset = read_csv(paths.dataset.renamed)

    reduced_dataset = DataFrame(index=encoded_dataset.index)
    label_encoders = LabelEncoders()

    # label and merge data
    label_and_merge = {
        'salary': {
            '10-13': '07-10',
            '13-99': '07-10',
        },
        'age': {
            '40-49': '30-39',
            '50-99': '30-39',
        },
        'education': {
            's_dip': 'low',
            'h_dip': 'low',
            's_ass': 'low',
            'h_ass': 'low',
            's_bac': 'medium',
            'h_bac': 'medium',
            's_mas': 'hight',
            'h_mas': 'hight',
            's_phd': 'hight',
            'h_phd': 'hight',
        },
        'experience': {
            '25-99': '15-25',
        }
    }

    for column_name, reduce_list in label_and_merge.items():
        print(f'labeling and merging: {column_name}')
        column = raw_dataset[column_name]
        for from_value, change_to in reduce_list.items():
            column = where(column == from_value, change_to, column)
        encoder = label_encoders.new_encoder(column_name)
        reduced_dataset[column_name] = encoder.fit_transform(column)

    # add column from encoded_dataset
    add_from_encoded_dataset = [
        'experience', 'born_state', 'work_state', 'co_workers_scaled',
        'hourly_paid', 'presence_at_work', 'primary_job', 'satisfaction'
    ]
    for column_name in add_from_encoded_dataset:
        print(f'adding from encoded_dataset: {column_name}')
        # TODO: load encoders too
        reduced_dataset[column_name] = encoded_dataset[column_name]

    # map multi_choice column and split them
    for column_name, data_structure in splitable_columns_reduced.items():
        print(f'spliting: {column_name}')
        reduced_dataset[column_name] = raw_dataset[column_name]
        func = create_binary_map_function(data_structure, column_name)
        split_and_encode_column(reduced_dataset, column_name, func)

    reduced_dataset.to_csv(paths.dataset.reduced, index=False)
    print(f'reduced dataset saved as: {paths.dataset.reduced}')


if __name__ == "__main__":
    if input('create encoded_dataset ? ').lower() in ['y', 'yes']:
        encode_dataset_97()
    if input('create reduced_dataset ? ').lower() in ['y', 'yes']:
        encode_and_reduce_dataset_97()
