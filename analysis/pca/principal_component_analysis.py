"""
Principle Component Analysis on dataset for feature extraction and reduction.
Original code:
https://statquest.org/2018/01/08/statquest-pca-in-python/#code
"""
from os.path import join
from sklearn import decomposition
import matplotlib.pyplot as plt
import numpy as np
import pandas
from config.config import encoded_full_path, pca_result_path

header = '+' * 80
df = pandas.read_csv(encoded_full_path)
df.dropna(inplace=True)
print(f'df shape: {df.shape}')


def plot(analysed_dataframe: pandas.DataFrame, labels: list,
         per_var: np.ndarray, name: str):
    # The following code constructs the Scree plot
    plt.figure(figsize=(10, 4))
    plt.bar(x=range(1, len(per_var)+1), height=per_var, tick_label=labels)
    plt.ylabel('Percentage of Explained Variance')
    plt.xlabel('Principal Component')
    plt.title('Scree Plot')
    scree_full_path = join(pca_result_path, name + '_Scree_Plot.png')
    plt.savefig(scree_full_path, dpi=500)
    plt.close()
    print(f'scree plot saved as: {scree_full_path}')

    # the following code makes a fancy looking plot using PC1 and PC2
    plt.scatter(analysed_dataframe.PC1, analysed_dataframe.PC2)
    plt.title('My PCA Graph')
    plt.xlabel('PC1 - {0}%'.format(per_var[0]))
    plt.ylabel('PC2 - {0}%'.format(per_var[1]))
    for sample in analysed_dataframe.index:
        pc1 = analysed_dataframe.PC1.loc[sample]
        pc2 = analysed_dataframe.PC2.loc[sample]
        plt.annotate(sample, (pc1, pc2))
    fancy_full_path = join(pca_result_path, name + '_Fancy.png')
    plt.savefig(fancy_full_path, dpi=500)
    plt.close()
    print(f'fancy plot saved as: {fancy_full_path}')


def do_pca(dataset: pandas.DataFrame, limit=None, name_prefix: str = None):
    print(header)
    file_name = f'{name_prefix}_with_{limit}_limit'

    pca = decomposition.PCA(limit)

    # 2D shape. (feature_dimension, sample_dimension,) or vise versa.
    analysed_ndarray: np.ndarray = pca.fit_transform(dataset)  # pca_data

    # 1D shape. (feature_dimension,)
    per_var: np.ndarray = np.round(pca.explained_variance_ratio_ * 100,
                                   decimals=1)

    labels = ['PC' + str(x) for x in range(1, len(per_var)+1)]

    # 2D shape. (feature_dimension, sample_dimension,) or vise versa.
    analysed_dataframe = pandas.DataFrame(analysed_ndarray,
                                          index=dataset.index,
                                          columns=labels)  # pca_df

    plot(analysed_dataframe, labels, per_var, file_name)

    # get top features
    features = {}
    for sample in analysed_dataframe.index:
        features[sample] = analysed_dataframe.PC1.loc[sample]
    features_sorted = sorted(features, key=lambda x: features[x],
                             reverse=True)
    top_features = ''
    for i in features_sorted[:20]:
        top_features += f'{i}: {features[i]}\n'

    # save some of data
    result = f'per_var:\n{per_var}\n\n'
    result += f'top features:\n{top_features}'
    top_full_path = join(pca_result_path, file_name + '_top.txt')
    with open(top_full_path, 'w') as my_file:
        my_file.write(result)
    print(f'top 20 columns and per_var saved as: {top_full_path}')


if __name__ == "__main__":
    do_pca(df.T, limit=None, name_prefix='dataset.T')

    new_df = df.copy()
    new_df.drop(columns=['co_workers'], inplace=True)
    do_pca(new_df.T, None, name_prefix='no_co_workers.T')

    new_df = df.copy()
    new_df.drop(columns=['co_workers', 'born_state', 'work_state'],
                inplace=True)
    do_pca(new_df.T, None, name_prefix='no_co_workers_no_state.T')

    new_df = df.copy()
    new_df.drop(columns=['co_workers', 'born_state', 'work_state'],
                inplace=True)
    do_pca(new_df.T, 0.95, name_prefix='no_co_workers_no_state.T')

    new_df = df.copy()
    new_df.drop(columns=['co_workers', 'born_state', 'work_state'],
                inplace=True)
    do_pca(new_df.T, 0.8, name_prefix='no_co_workers_no_state.T')

    new_df = df.copy()
    new_df.drop(columns=['co_workers', 'born_state', 'work_state',
                'hourly_paid'], inplace=True)
    do_pca(new_df.T, None, name_prefix='no_co_workers_no_state_no_hourly.T')

    new_df = df.copy()
    new_df.drop(columns=['co_workers', 'born_state', 'work_state',
                'hourly_paid'], inplace=True)
    do_pca(new_df.T, 0.8, name_prefix='no_co_workers_no_state_no_hourly.T')
