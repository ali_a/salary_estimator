from os.path import join
from config.config import paths
from pandas import read_csv


def create_corr(name: str, dataset):
    corr = dataset.corr()
    full_path = join(paths.analysis.correlation.results, f'{name}.csv')
    corr.to_csv(full_path)
    print(f'correlation saved as: {full_path}')

    salary_corr = corr[['salary']]
    salary_corr.index = corr.index
    sorted_salary = salary_corr.sort_values(by='salary', ascending=False)
    full_path = join(paths.analysis.correlation.results, f'{name}_salary.csv')
    sorted_salary.to_csv(full_path)
    print(f'correlation with salary saved as: {full_path}')


if __name__ == '__main__':
    dataset = read_csv(paths.dataset.encoded)
    create_corr('encoded', dataset)
    reduced_dataset = read_csv(paths.dataset.reduced)
    create_corr('reduced', reduced_dataset)
